<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserPegawai;
use App\Models\Role;

class UserPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = UserPegawai::all();
        return view('userpegawai.userpegawai', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new UserPegawai;
        $roles = Role::all();
        return view('userpegawai.create', compact('model', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['firstname' => 'required',
    'nik' => 'required',
    'email' => 'required',
    'password' => 'required','position' => 'required']);

        $model = new UserPegawai;
        $model -> nik = $request->nik;
        $model -> firstname = $request->firstname;
        $model -> lastname = $request->lastname;
        $model -> gender = $request->gender;
        $model -> email = $request->email;
        $model -> password = bcrypt($request->password);
        $model -> position = $request->position;
        $model -> dob = $request->dob;
        $model -> birth_place = $request->birth_place;
        $model -> handphone = $request->handphone;
        $model -> alamat = $request->alamat;
        $model -> kode_pos = $request->kode_pos;
        $model -> marital_status = $request->marital_status;
        $model -> religion = $request->religion;
        $model -> status = $request->status;
        $model -> id_role = $request->id_role;
        $model -> cuti = 12;
        $model -> is_active = true;
        $model->save();

        if($model){
            return redirect()
                ->route('userpegawai.index')
                ->with([
                    'success' => 'Berhasil tambahkan data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal tambahkan data'
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = UserPegawai::findOrFail($id);
        $roles = Role::all();
        return view('userpegawai.edit', compact('model','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['firstname' => 'required',
        'nik' => 'required',
        'email' => 'required',
        'position' => 'required']);

        $model = UserPegawai::find($id);
        $model -> nik = $request->nik;
        $model -> firstname = $request->firstname;
        $model -> lastname = $request->lastname;
        $model -> gender = $request->gender;
        $model -> email = $request->email;
        $model -> position = $request->position;
        $model -> dob = $request->dob;
        $model -> birth_place = $request->birth_place;
        $model -> handphone = $request->handphone;
        $model -> alamat = $request->alamat;
        $model -> kode_pos = $request->kode_pos;
        $model -> marital_status = $request->marital_status;
        $model -> religion = $request->religion;
        $model -> status = $request->status;
        $model -> id_role = $request->id_role;
        $model -> is_active = true;
        $model->save();

        if($model){
            return redirect()
                ->route('userpegawai.index')
                ->with([
                    'success' => 'Berhasil ubah data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal ubah data'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = UserPegawai::findOrFail($id);
        $model->delete();
        if($model){
            return redirect()
                ->route('userpegawai.index')
                ->with([
                    'success' => 'Berhasil hapus data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal hapus data'
                ]);
        }
    }
}
