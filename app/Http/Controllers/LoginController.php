<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.newindex', [
            'title' => 'Login'
        ]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);

        if(Auth::attempt($credentials)){
            $request->session()->regenerate();
            return redirect()->intended('/dashboard');
        }
        return back()->with('error', 'Login Gagal!');
    }

    public function afterLogin()
    {
        $permanen = collect(DB::SELECT("select count(id) as jumlah_permanen from m_user
        where status = 'Permanen'"))->first();
        $kontrak = collect(DB::SELECT("select count(id) as jumlah_kontrak from m_user
        where status = 'Kontrak'"))->first();
        $magang = collect(DB::SELECT("select count(id) as jumlah_magang from m_user
        where status = 'Magang'"))->first();
        $outsource = collect(DB::SELECT("select count(id) as jumlah_outsource from m_user
        where status = 'Outsource'"))->first();

        $pria = collect(DB::SELECT("select count(id) as jumlah_pria from m_user
        where gender = 'Pria'"))->first();
        $wanita = collect(DB::SELECT("select count(id) as jumlah_wanita from m_user
        where gender = 'Wanita'"))->first();
        return view('dashboard.dashboard', compact('permanen', 'kontrak', 'magang', 'outsource','pria','wanita'));
    }

    public function logout()
    {
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/login');

    }
}
