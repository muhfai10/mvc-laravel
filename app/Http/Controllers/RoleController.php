<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Role::all();
        return view('role.role', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Role;
        return view('role.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['rolename' => 'required']);

        $model = new Role;
        $model -> rolename = $request->rolename;
        $model -> keterangan = $request->keterangan;
        $model->save();

        if($model){
            return redirect()
                ->route('role.index')
                ->with([
                    'success' => 'Berhasil tambahkan data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal tambahkan data'
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Role::findOrFail($id);
        return view('role.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['rolename' => 'required']);

        $model = Role::find($id);
        $model -> rolename = $request->rolename;
        $model -> keterangan = $request->keterangan;
        $model->save();

        if($model){
            return redirect()
                ->route('role.index')
                ->with([
                    'success' => 'Berhasil ubah data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal ubah data'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Role::findOrFail($id);
        $model->delete();
        if($model){
            return redirect()
                ->route('role.index')
                ->with([
                    'success' => 'Berhasil hapus data'
                ]);
        }else{
            return redirect()
                ->back
                ->withInput()
                ->with([
                    'error' => 'Gagal hapus data'
                ]);
        }
    }
}
