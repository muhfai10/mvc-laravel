<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class UserPegawai extends \Illuminate\Foundation\Auth\User
{
    use HasFactory;
    protected $table = "m_user";

    protected $hidden = [
        'password', 'remember_token',
    ];
}
