@extends('layouts.index')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-4">
    <br>
                @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-danger" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif
    <form class="form-signin" action="/login" method="post">
        @csrf
        <br>
  <h1 class="h3 mb-3 font-weight-normal text-center"><strong>Please Login </strong></h1>
  <label for="email" class="sr-only">Email address</label>
  <input type="email" id="email" class="form-control @error('email')
      is-invalid @enderror" name="email" value="{{old('email')}}" placeholder="Email address" required autofocus>

      @error('email')
          <div class="invalid-feedback d-block">
            {{ $message}}
          </div>
      @enderror

  <label for="password" class="sr-only">Password</label>
  <input type="password" id="password"  name="password" class="form-control" placeholder="Password" required>

  <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
  <small class="text-center"><a href="/register">Register Now!</a></small>
</form>
 
    </div>
</div>

<script>
// setTimeout( myStopFunction, 2500);

// function myStopFunction() {
//  $('#success-alert').hide();
// }

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
    </script>

<script>
// setTimeout( myStopFunction, 2500);

// function myStopFunction() {
//  $('#success-alert').hide();
// }

$("#error-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#error-alert").slideUp(500);
});
    </script>

@endsection