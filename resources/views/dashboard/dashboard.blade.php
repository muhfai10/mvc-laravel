@extends('layouts.maindashboard')

@section('content-header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>My Dashboard</h1>
          </div>
          <!-- <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
            </ol>
          </div> -->
        </div>
      </div><!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="row">
      <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box">
              <span class="info-box-icon"> <img src="/adminlte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"></span>

              <div class="info-box-content">
                <span class="info-box-text">{{auth()->user()->firstname}} {{auth()->user()->lastname}}</span>
                <span class="info-box-number">{{auth()->user()->position}} | {{auth()->user()->status}} | Sisa Cuti : {{auth()->user()->cuti}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
      </div>

      <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box">
      <span class="info-box-icon bg-success"><i class="fas fa-location-arrow"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Jakarta | {{ date('d-m-Y', strtotime(now())) }}</span>
        <span class="info-box-number">Temperature: 26C</span>
      </div>
      </div>
      </div>


      </div>
      </div>
@endsection

@section('content')
@if(auth()->user()->position == 'HR Development')
<div class="container-fluid">

<h5 class="mb-2">Info Rekrutmen</h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Pelamar</span>
                <span class="info-box-number">30</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Lulus Seleksi</span>
                <span class="info-box-number">7</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="far fa-hourglass"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Dalam Proses</span>
                <span class="info-box-number">4</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><i class="fas fa-remove"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Gagal Seleksi</span>
                <span class="info-box-number">2</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
        
@else
        <div class="container-fluid">
<h5 class="mb-2">Info Penugasan</h5>
        <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="fas fa-tasks"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Tugas</span>
                <span class="info-box-number">8</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="far fa-check-square"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Selesai</span>
                <span class="info-box-number">4</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="far fa-hourglass"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Sedang Diproses</span>
                <span class="info-box-number">2</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-secondary"><i class="fas fa-archive"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Belum Dikerjakan</span>
                <span class="info-box-number">2</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
        @endif
        <div class="container-fluid">
<h5 class="mb-2">Info Karyawan</h5>
<div class="row">
          <div class="col-md-6">
             <!-- DONUT CHART -->
             <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Data Statistik Status Karyawan</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
                <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-6">
             <!-- PIE CHART -->
             <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Data Karyawan Berdasarkan Gender</h3>

                <!-- <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div> -->
              </div>
              <div class="card-body">
                <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
</div>
</div>

<!-- <div class="card">
        <div class="card-header">
          <h3 class="card-title">Title</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          Start creating your amazing application!
        </div>
        <!-- /.card-body -->
        
        <!-- /.card-footer-->
</div> -->

<script>
   $(function () {

     //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var permanen = {!! json_encode($permanen->jumlah_permanen, JSON_HEX_TAG) !!};
    var kontrak = {!! json_encode($kontrak->jumlah_kontrak, JSON_HEX_TAG) !!};
    var magang ={!! json_encode($magang->jumlah_magang, JSON_HEX_TAG) !!};
    var outsource ={!! json_encode($outsource->jumlah_outsource, JSON_HEX_TAG) !!};
    var donutData        = {
      labels: [
          'Permanen',
          'Kontrak',
          'Magang',
          'Outsource',
      ],
      datasets: [
        {
          data: [permanen,kontrak,magang,outsource],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })

     //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pria ={!! json_encode($pria->jumlah_pria, JSON_HEX_TAG) !!};
    var wanita ={!! json_encode($wanita->jumlah_wanita, JSON_HEX_TAG) !!};
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = {
      labels: [
          'Pria',
          'Wanita',
      ],
      datasets: [
        {
          data: [pria, wanita],
          backgroundColor : ['#39B5E0', '#FF78F0'],
        }
      ]
    }
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

  })
</script>



@endsection
