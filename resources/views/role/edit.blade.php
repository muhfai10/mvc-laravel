@extends('layouts.index')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <!-- Notifikasi menggunakan flash session data -->
                @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-error" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif

                <div class="card border-0 shadow rounded">
                <div class="card-header">
                     <h5 class="card-title"><strong>Form Ubah Role</strong></h5>
                    </div>
                    <div class="card-body">

                        <form action="{{ route('role.update', $model->id) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="rolename">Nama Role</label>
                                <input type="text" class="form-control @error('rolename') is-invalid @enderror"
                                    name="rolename" value="{{ old('rolename', $model->rolename) }}" required>

                                <!-- error message untuk rolename -->
                                @error('rolename')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <input type="text" class="form-control @error('keterangan') is-invalid @enderror"
                                    name="keterangan" value="{{ old('keterangan', $model->keterangan) }}" required>

                                <!-- error message untuk keterangan -->
                                @error('keterangan')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-md btn-primary">Update</button>
                            <a href="{{ route('role.index') }}" class="btn btn-md btn-secondary">back</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
    </script>

@endsection