@extends('layouts.maindashboard')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <!-- Notifikasi menggunakan flash session data -->
                @if (session('success'))
                <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('success') }}</strong>
                </div>
                @endif

                @if (session('error'))
                <div class="alert alert-error" id="error-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ session('error') }}</strong>
                </div>
                @endif

                <div class="card border-0 shadow rounded">
                <div class="card-header">
                     <h1 class="card-title"><strong>Form Ubah User Pegawai</strong></h1>
                    </div>
                    <div class="card-body">

                        <form action="{{ route('userpegawai.update', $model->id) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="nik">NIK</label>
                                <input type="text" class="form-control"
                                    name="nik" value="{{ old('nik', $model->nik) }}" >
                            </div>
                            <div class="form-group col-md-4">
                                <label for="firstname">Nama Awal</label>
                                <input type="text" class="form-control @error('firstname') is-invalid @enderror"
                                    name="firstname" value="{{ old('firstname', $model->firstname) }}" required>

                                <!-- error message untuk title -->
                                @error('firstname')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-4">
                                <label for="lastname">Nama Akhir</label>
                                <input type="text" class="form-control"
                                    name="lastname" value="{{ old('lastname', $model->lastname) }}" >
                            </div>

                            <div class="form-group col-md-4">
                                <label for="position">Posisi Pekerjaan</label>
                                <input type="text" class="form-control @error('position') is-invalid @enderror"
                                    name="position" value="{{ old('position', $model->position) }}" required>

                                <!-- error message untuk title -->
                                @error('position')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-4">
                                <label for="email">Email</label>
                                <input type="text" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email', $model->email) }}" required>

                                <!-- error message untuk title -->
                                @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group col-md-4">
                                <label for="password">Password</label>
                                <input type="password" class="form-control"
                                    name="password" value="{{ old('password', $model->password) }}" disabled>
                            </div>

                            <div class="form-group col-md-4">
                            <label for="id_role">Role User</label>
                            <select id="id_role" class="form-control" name="id_role" value="{{ old('id_role', $model->id_role ) }}">
                                <option hidden>Pilih Role</option>
                                @foreach ($roles as $role)
                                <option value="{{$role->id}}" {{ $model->id_role == $role->id ? 'selected': '' }}>{{$role->rolename}}</option>
                                @endforeach
                                
                            </select>
                            </div>

                            <div class="form-group col-md-4">
                            <label for="gender">Gender</label>
                            <select id="gender" class="form-control" name="gender" value="{{ old('gender', $model->gender) }}">
                                <option hidden>Pilih Gender</option>
                                <option value="Pria" {{$model->gender == 'Pria' ? 'selected': '' }}>Pria</option>
                                <option value="Wanita" {{$model->gender == 'Wanita' ? 'selected': '' }}>Wanita</option>
                            </select>
                            </div>

                            <div class="form-group col-md-4">
                            <label for="dob">Tanggal Lahir</label>
                                <input type="text" class="form-control"
                                    name="dob" value="{{ old('dob', $model->dob) }}" >
                            </div>

                            <div class="form-group col-md-4">
                            <label for="birth_place">Tempat Lahir</label>
                                <input type="text" class="form-control"
                                    name="birth_place" value="{{ old('birth_place', $model->birth_place) }}" >
                            </div>

                            <div class="form-group col-md-8">
                            <label for="alamat">Alamat Tempat Tinggal</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" name="alamat" onKeyPress class="form-control"  rows="3">{{ old('alamat', $model->alamat) }}</textarea>
                            </div>

                            <div class="form-group col-md-4">
                            <label for="kode_pos">Kode Pos</label>
                                <input type="text" class="form-control"
                                    name="kode_pos" value="{{ old('kode_pos', $model->kode_pos) }}" >
                            </div>

                            <div class="form-group col-md-4">
                            <label for="handphone">No. Handphone</label>
                                <input type="text" class="form-control"
                                    name="handphone" value="{{ old('handphone', $model->handphone) }}" >
                            </div>

                            <div class="form-group col-md-4">
                            <label for="marital_status">Status Pernikahan</label>
                            <select id="marital_status" class="form-control" name="marital_status" value="{{ old('marital_status', $model->marital_status) }}">
                                <option hidden>Pilih Status Pernikahan</option>
                                <option value="Single" {{$model->marital_status == 'Single' ? 'selected': '' }}>Single</option>
                                <option value="Menikah" {{$model->marital_status == 'Menikah' ? 'selected': '' }}>Menikah</option>
                                <option value="Divorce" {{$model->marital_status == 'Divorce' ? 'selected': '' }}>Divorce</option>
                            </select>
                            </div>

                            <div class="form-group col-md-4">
                            <label for="religion">Agama</label>
                            <select id="religion" class="form-control" name="religion" value="{{ old('religion', $model->religion) }}">
                                <option hidden>Pilih Agama</option>
                                <option value="Islam" {{$model->religion == 'Islam' ? 'selected': '' }}>Islam</option>
                                <option value="Kristen" {{$model->religion == 'Kristen' ? 'selected': '' }}>Kristen</option>
                                <option value="Katholik" {{$model->religion == 'Katholik' ? 'selected': '' }}>Katholik</option>
                                <option value="Hindu" {{$model->religion == 'Hindu' ? 'selected': '' }}>Hindu</option>
                                <option value="Buddha"  {{$model->religion == 'Buddha' ? 'selected': '' }}>Buddha</option>
                                <option value="Kong Hu Chu"   {{$model->religion == 'Kong Hu Chu' ? 'selected': '' }}>Kong Hu Chu</option>
                            </select>
                            </div>

                            <div class="form-group col-md-4">
                            <label for="status">Status Karyawan</label>
                            <select id="status" class="form-control" name="status" value="{{ old('status', $model->status) }}">
                                <option hidden>Pilih Status Karyawan</option>
                                <option value="Permanen" {{$model->status == 'Permanen' ? 'selected': '' }}>Permanen</option>
                                <option value="Kontrak"  {{$model->status == 'Kontrak' ? 'selected': '' }}>Kontrak</option>
                                <option value="Magang"  {{$model->status == 'Magang' ? 'selected': '' }}>Magang</option>
                                <option value="Outsource" {{$model->status == 'Outsource' ? 'selected': '' }}>Outsource</option>
                            </select>
                            </div>

                            </div>
                            <button type="submit" class="btn btn-md btn-primary">Save</button>
                            <a href="{{ route('userpegawai.index') }}" class="btn btn-md btn-secondary">back</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>


$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
});
    </script>


@endsection